from odoo import models, fields, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    social_facebook = fields.Char(string="Facebook")
    social_twitter = fields.Char(string="Twitter")
    social_linkedin = fields.Char(string="LinkedIn")
    preferred_contact_metod = fields.Selection([
        ('email', 'Email'),
        ('phone', 'Phone'),
        ('sms', 'SMS'),
        ('none', 'None'),
    ], string='Prefered Contact Method', default='email')
    customer_segment = fields.Selection([
        ('vip', 'VIP'),
        ('reguler', 'Reguler'),
        ('new', 'New'),
    ], string='Customer')