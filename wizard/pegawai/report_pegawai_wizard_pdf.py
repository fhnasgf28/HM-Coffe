from odoo import models, fields, api, _


class EmployeeReportWizard(models.TransientModel):
    _name = 'pegawai.report.wizard.pdf'
    _description = 'Employee Report Wizard pdf'

    hire_date_start = fields.Date(string="Hire Date Start")
    hire_date_end = fields.Date(string="Hire Date End")

    def generate_pdf_report(self):
        data = {
            'start_date': self.start_date,
            'end_date': self.end_date,
        }

        self.env.ref('hm_coffe.pegawai_pdf_report').sudo().report_file = 'Data pegawai'

        action = self.env.ref('hm_coffe.pegawai_pdf_report').report_action(self, data=data)
        action.update({'close_on_report_download': True})

        return action

    def get_employees(self):
        return self.env['caffe.pegawai'].search([
            ('hire_date', '>=', self.start_date),
            ('hire_date', '<=', self.end_date)
        ])
