from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import pytz
import locale

locale.setlocale(locale.LC_ALL, '')


class PegawaiReport(models.AbstractModel):
    _name = 'report.hm_coffe.action_pegawai_report_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    _description = "Model for generating excel report"

    def generate_xlsx_report(self, workbook, data, wizard):
        sheet = workbook.add_worksheet('Employee Report')
        bold = workbook.add_format({'bold': True})

        sheet.write(0, 0, 'Name', bold)
        sheet.write(0, 1, 'Job Position', bold)
        sheet.write(0, 2, 'Work Email', bold)
        sheet.write(0, 3, 'Work Phone', bold)

        employees = self.env['coffe.pegawai'].search([
            ('hire_date', '>=', wizard.start_date),
            ('hire_date', '<=', wizard.end_date),
        ])

        row = 1

        for employee in employees:
            sheet.write(row, 0, employee.name)
            sheet.write(row, 1, employee.email)
            sheet.write(row, 2, employee.address)
            sheet.write(row, 3, employee.phone)