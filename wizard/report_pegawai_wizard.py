from odoo import models, fields, api, _


class EmployeeReportWizard(models.TransientModel):
    _name = 'employee.report.wizard'
    _description = 'Employee Report Wizard'

    start_date = fields.Date(string="Start Date", required=True)
    end_date = fields.Date(string="End Date", required=True)

    def action_print_report(self):
        print('method ini diklik')
        return self.env.ref('hm_coffe.action_pegawai_report_xlsx').report_action(self)
