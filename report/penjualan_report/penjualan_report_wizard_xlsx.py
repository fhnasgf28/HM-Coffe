from odoo import models, fields, api, _


class PenjualanReport(models.AbstractModel):
    _name = 'penjualan.report.wizard'
    _description = "Wizard for generate payment report"

    from_date = fields.Datetime(default=lambda self: fields.Datetime.now())
    to_date = fields.Datetime(default=lambda self: fields.Datetime.now())
    def print_excel(self):
        print('method ini di klik')
        from_date = self.from_date
        to_date = self.to_date

        data = {
            "from_date": from_date,
            "to_date": to_date
        }

        self.env.ref('hm_coffe.action_penjualan_report').sudo().report_file = 'Penjualan Report'

        action = self.env.ref('hm_coffe.action_penjualan_report').report_action(self, data=data)
        action.update({'close_on_report_download': True})

        return action

